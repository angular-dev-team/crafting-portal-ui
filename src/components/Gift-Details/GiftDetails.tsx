import React from 'react';
import IGiftdetailsProps from '../../interfaces/IGiftdetailsProps';
import { connect } from "react-redux";

class GiftdetailsComponents extends React.Component<IGiftdetailsProps>{
    constructor(props:IGiftdetailsProps) {
        super(props);
        this.state = {
            
        };
    }
    render() {
        return(
            <div className="container">
                {this.props.selectedCategoryID}
            </div>
        );
    }
}

function mapStateToProps(state:any, ownProps:any) {
    return {
      selectedCategoryID: state.categoryState.selectedCategoryId
    };
  }
export default connect(mapStateToProps)(GiftdetailsComponents);