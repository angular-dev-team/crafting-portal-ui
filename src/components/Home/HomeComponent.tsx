import React from 'react';
import { connect } from "react-redux";
import IHomeProps from '../../interfaces/IHomeProps';
import './Home.css';
import TilesView from '../../common/Tiles/Tiles';
import { ICategory, ITilesViewProps } from '../../interfaces/ITilesViewProps';
import Banner from '../Banner/Banner';
import { dispatchStoreAction } from '../../utils/reduxUtils';
import { SET_SELECTED_CATEGORY_ID, SET_CATEGORIES } from '../../actions/actionTypes';

interface IHomeState {
    categories: Array<ICategory>
}

class HomeComponent extends React.Component<IHomeProps, IHomeState> {
    constructor(props: any) {
        super(props);
        this.state = {
            categories: this.props.categories
        };
        this.categoryClickHandler = this.categoryClickHandler.bind(this);
    }
    componentDidMount() {
        dispatchStoreAction(SET_CATEGORIES,{});
    }
    componentWillReceiveProps(nextProps: any) {
        const categories = nextProps.categories;
        this.setState({ ...this.state, categories });
    }
    categoryClickHandler(e:React.MouseEvent, id: string): void {
        e.preventDefault();
        // this.props.setSelectedCategoryId({
        //     payload:{id}
        // });
        dispatchStoreAction(SET_SELECTED_CATEGORY_ID, id);
        this.props.history.push("/gift-details");
    }
    getTilesViewProps(): ITilesViewProps {
        const { categories } = this.state;
        return {
            categories,
            categoryClickHandler: this.categoryClickHandler
        }
    }
    render() {
        const tilesProps = this.getTilesViewProps();
        return (
            <div className="container">

                <div className="home-container">
                    <Banner />
                    <TilesView {...tilesProps} />
                </div>
            </div>
        );
    }
}

function mapStateToProps(state: any, ownProps: any) {
    return {
        categories: state.categoryState.categories
    };
}
/* function mapDispatchToProps(dispatch: any) {
    return {
        setSelectedCategoryId: bindActionCreators(actions.setSelectedCategoryId, dispatch),
        setCategories: bindActionCreators(actions.setCategories, dispatch)
    };
} */
export default connect(mapStateToProps)(HomeComponent);