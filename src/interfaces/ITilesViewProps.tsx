export interface ICategory {
    id: string
    name: string
}
export interface ITilesViewProps {
    categories?: Array<ICategory>
    categoryClickHandler?: (e: React.MouseEvent, id:string) => void
}
