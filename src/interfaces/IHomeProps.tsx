import { RouteComponentProps } from 'react-router-dom';
import {ITilesViewProps} from './ITilesViewProps';

export default interface IHomeProps extends RouteComponentProps<any> {
    selectedCategoryId: string,
    tilesViewProps: ITilesViewProps,
    setSelectedCategoryId: any,
    setCategories: any,
    categories: any
}