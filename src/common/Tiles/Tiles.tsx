import React from 'react';
import { ICategory, ITilesViewProps } from '../../interfaces/ITilesViewProps';

const TilesView: React.FC<ITilesViewProps> = (props) => {
    const {categories = []} = props;
    return (
        <div className="cp-row">
            {
                categories.map((tile: ICategory) => {
                    return <div className="cp-col" key={tile.name}>
                        <div>
                            <p onClick={(e)=> handleClick(e,props, tile.id)}>{tile.name}</p>
                        </div>
                    </div>
                })
            }
        </div>
    );
}

function handleClick(e: React.MouseEvent,props:ITilesViewProps, id: string,) {
    if(props.categoryClickHandler) {
        props.categoryClickHandler(e,id);
    }
}
export default TilesView;