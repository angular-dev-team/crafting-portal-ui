import {combineReducers} from 'redux';
import {categoryReducer} from './categoryReducer';
const rootReducer = combineReducers({
    categoryState:categoryReducer
})

export default rootReducer;